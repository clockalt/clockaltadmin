import Vue from 'vue'
import App from './App.vue'

import VueRouter from 'vue-router'
import axios from 'axios'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.prototype.$http = axios

const router = new VueRouter({
  routes:[
   
  ]
})
new Vue({
  router,
  render: h => h(App),
}).$mount('#wrapper')
